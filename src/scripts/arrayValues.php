<?php
//ZAD 2

function arrayValues($arr, $arrKeys, $method) {
    if(($method<=2)&&($method>0)) {
        $text = "";
        if($method==1) {
            include('./src/scripts/additional/nestedArray.php');
            $result = nestedArray($arr, $arrKeys);
        }
    
        if($method==2) {
            include('./src/scripts/additional/mapArray.php');
            $result = mapArray($arr, $arrKeys);
        }

        if(isset($result)) {
            foreach($result as $r) {
                $text .= "Name: ".$r['name']." | "."Price: ".$r['price'].PHP_EOL;
            }
            //echo $text; or:
            echo nl2br($text); 
        }

    } else echo "Unexpected value for variable: method.";
   
}