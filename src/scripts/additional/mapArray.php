<?php 

function mapArray($array, $results) {
    $ids = array_map(function($value) {
        return $value['result_id'];
    }, $results);
    
    $resultArray = array_filter($array, function($value) use ($ids) {
        return in_array($value['id'], $ids);
    });

    return $resultArray;
}

