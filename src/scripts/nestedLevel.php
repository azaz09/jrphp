<?php 
//ZAD 3
require_once("additional/cats.php");

function viewNestedLvl($i) {
    $txt = PHP_EOL."NestedLvl [".$i."] => ".PHP_EOL."id: ";
    //echo $txt;
    echo nl2br($txt);
}

function countNestedLvl($v, $i) {
    foreach($v as $sub) {
        viewNestedLvl($i);
        echo current($sub);
        if(isMultiArray($sub)) {
            $i++;
            countNestedLvl(next($sub), $i);
            if(is_array(current($sub)))  $i--;
        }
       
    }
}

function isMultiArray( $arr ) { 
    rsort( $arr ); 
    return isset( $arr[0] ) && is_array( $arr[0] ); 
} 