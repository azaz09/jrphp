<?php
//ZAD 1
function literuj($str) {
    $len = mb_strlen($str, 'UTF-8');
    $text = '';
    for($i = 0; $i < $len; $i++) {
        $text .= $i.' - '.mb_substr($str, $i, 1, 'UTF-8').PHP_EOL;
    }
    //echo $text; or:
    echo nl2br($text);
}
