<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="./src/css/style.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>

<body>
    <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="container px-0">
        <nav class="navbar navbar-expand bg-success rounded-bottom mb-2">
            <a class="navbar-brand p-1" href="#">jrPHP</a>
            <div class="collapse navbar-collapse text-uppercase d-inline-flex justify-content-end" id="navbarNav">
                <div class="navbar-nav d-inline-flex justify-content-end">
                    <div class="nav-item b-dotted-r">
                        <a class="nav-link" href="#zad1">Zad 1</a>
                    </div>
                    <div class="nav-item b-dotted-r">
                        <a class="nav-link" href="#zad2">Zad 2</a>
                    </div>
                    <div class="nav-item ">
                        <a class="nav-link" href="#zad3">Zad 3</a>
                    </div>
                </div>
            </div>
        </nav>
        <section id="breadcrumbs m-2 mb-5">
            <div>
                <a class="text-dark p-2" href="https://drive.google.com/file/d/1L1MFWriNri1KeBQVKWfD_JGg8y4smXZN/view" target="_blank"><ins>pdf: jr PHP</ins></a> /
                <a class="text-dark p-2" href="https://bitbucket.org/azaz09/jrphp/src/master/" target="_blank"><ins>bitbucket</ins></a>
            </div>
        </section>
        <section id="zad1" class="p-1 d-column-flex m-2">
            <div class="">
                <h2>Zadanie 1</h2>
            </div>
            <div class="d-inline-flex">
                <div>
                    <div class="pl-4">
                        <h5>wywołanie 1</h5>
                    </div>
                    <div class="pl-5">
                        <p>
                            <?php
                    require_once('./src/scripts/spellWordsLetters.php');
                    #wywołanie 1 
                    literuj('lorem ipsum dolor sit amet');
                    ?>
                        </p>
                    </div>
                </div>
                <div>
                    <div class="pl-4">
                        <h5>wywołanie 2</h5>
                    </div>
                    <div class="pl-5">
                        <p>
                            <?php 
                        literuj('zażółć gęślą jaźń');
                    ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <hr/>
        <section id="zad2" class="p-1 d-column-flex m-2 font-weight-lighter">
            <div class="">
                <h2>Zadanie 2</h2>
                <?php 
                    include_once('./src/scripts/arrayValues.php');
                    include_once('./src/scripts/additional/arrays.php');
                ?>
            </div>
            <div class="d-lg-inline-flex d-md-inline-flex">
                <div>
                    <div class="pl-4">
                        <h5>Sposób 1</h5>
                    </div>
                    <div class="pl-5">
                        <p>
                            <?php
                                arrayValues($array, $results, 1);
                            ?>
                        </p>
                    </div>
                </div>
                <div>
                    <div class="pl-4">
                        <h5>Sposób 2 (Preferowany)</h5>
                    </div>
                    <div class="pl-5">
                        <p>
                            <?php 
                                arrayValues($array, $results, 2);
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <hr/>
        <section id="zad3" class="p-1 d-column-flex m-2 font-weight-lighter">
            <div class="">
                <h2>Zadanie 3</h2>
            </div>
            <div class="pl-4">
                <p>
                    <?php
                    require_once('./src/scripts/nestedLevel.php');
                    countNestedLvl($cats, 0);
                    ?>
                </p>
                <p>
                    <?php 
                    
                    ?>
                </p>

            </div>

        </section>
    </div>




    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
</body>

</html>